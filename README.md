# devops-netology 
 В .gitignore будут проигнорированы все файлы из каталога .terraform, где бы он не находился.
 также все файлы по маскам: 
 *.tfstate
 *.tfstate.*
 crash.log
 crash.*.log
 *.tfvars
 *.tfvars.json
 override.tf
 override.tf.json
 *_override.tf
 *_override.tf.json
 .terraformrc
 terraform.rc

